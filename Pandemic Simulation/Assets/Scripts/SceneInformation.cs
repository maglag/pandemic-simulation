using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneInformation : MonoBehaviour
{
    public string time_passed;
    public string actual_time_passed;
    public string avg_fps;
    public float timescale;
    private float relative_time;
    private int world_min;
    private int world_sec;
    private int real_min;
    private int real_sec;
    private List<GameObject> climbers;
    public List<GameObject> infected_climbers;
    //public int infected_count;
    
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] temp = GameObject.FindGameObjectsWithTag("Climber");
        climbers = new List<GameObject>(temp);
        infected_climbers = new List<GameObject>();
        timescale = 20f;
        Time.timeScale = timescale;
    }

    
    // Update is called once per frame
    void Update()
    {
        foreach (var climber in climbers)
        {
            if (climber.GetComponent<AgentClass>().infected == 1 && !infected_climbers.Contains(climber))
            {
                Debug.Log("Added " + climber.name + " to the list");
                infected_climbers.Add(climber);
            } 
        }
        
        relative_time = Time.time / timescale;
        world_min = Mathf.FloorToInt(Time.time / 60);
        world_sec = Mathf.FloorToInt(Time.time % 60);
        real_min = Mathf.FloorToInt(relative_time / 60);
        real_sec = Mathf.FloorToInt(relative_time % 60);
        time_passed = world_min.ToString("00") + ":" + world_sec.ToString("00");
        actual_time_passed = real_min.ToString("00") + ":" + real_sec.ToString("00");
        avg_fps = (Time.frameCount / relative_time).ToString("#.00");
        
    }
}
