using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.AI;

public class AgentClass : MonoBehaviour
{
    
    public int skill_level; // Skill level from 1-4
    public int stamina; // How many attempts
    public bool vaccinated; // false = Unvaccinated, true = vaccinated
    public int infected; // 0 = No, 1 = Got infected (not infectious because of incubation), 2 = Infectious
    public bool tried_infected;
    public bool isStopped;

    public Material mat;
    public MeshRenderer mesh;
    private Material healthyMat;
    private Material vaccinatedMat;
    private Material infectedMat;
    private Material infectiousMat;


    // Setters
    public void SetInfected(int given_infected)
    {
        infected = given_infected;
        //Display_Infected = infected;
    }

    // 0 = false, 1 = true
    public void SetTriedInfected(int value)
    {
        if (value == 0)
        {
            tried_infected = false;
            //Debug.Log(gameObject.name + " tried_infected value set to false");
        }
        else if (value == 1)
        {
            tried_infected = true;
            //Debug.Log(gameObject.name + " tried_infected value set to true");
        }
    }

    // Decreases stamina by 1
    public void DecreaseStamina()
    {
        stamina -= 1;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        healthyMat = Resources.Load<Material>("Materials/Infection_Related/Healthy");
        vaccinatedMat = Resources.Load<Material>("Materials/Infection_Related/Vaccinated");
        infectedMat = Resources.Load<Material>("Materials/Infection_Related/Infected");
        infectiousMat = Resources.Load<Material>("Materials/Infection_Related/Infectious");
        mesh = gameObject.GetComponent<MeshRenderer>();
        tried_infected = false;
        isStopped = gameObject.GetComponent<NavMeshAgent>().isStopped;
        // Randomly assign a skill level to the climber:
        float rand = Random.value;
        // Use the random value to come up with a probabilistic distribution of skill level
        if (rand <= 0.2)
        {
            skill_level = 1;
            stamina = 15;
        }
        else if (rand > 0.2 && rand <= 0.55)
        {
            skill_level = 2;
            stamina = 20;
        }
        else if (rand > 0.55 && rand <= 0.85)
        {
            skill_level = 3;
            stamina = 25;
        }
        else
        {
            skill_level = 4;
            stamina = 35;
        }

        // SET VACCINATION AND INFECTION RATE
        SetVaccinationRate(0.7);
        SetInfectionRate(0.2);
        SetMaterial(this, vaccinated, infected);

    }

    public void SetMaterial(AgentClass agent, bool vaccinated, int infected)
    {
        if (!vaccinated && infected == 0)
        {
            mat = healthyMat;
            mesh.material = mat;
        }
        else if (vaccinated && infected == 0)
        {
            mat = vaccinatedMat;
            mesh.material = mat;
        }
        else if (infected == 1)
        {
            mat = infectedMat;
            mesh.material = mat;
        }
        else if (infected == 2)
        {
            mat = infectiousMat;
            mesh.material = mat;
        }
    }
    
    public void SetVaccinationRate(double ratio)
    {
        float rand2 = Random.value;
        if (rand2 <= ratio)
        {
            vaccinated = true;
        }
        else if (rand2 > ratio)
        {
            vaccinated = false;
        }
    }

    public void SetInfectionRate(double ratio)
    {
        // Out of all infected climbers in the gym, 75% are infectious and 25% are infected but not infectious, this is easily adjustable
        float rand3 = Random.value;
        if (rand3 > ratio)
        {
            infected = 0;
        }
        else if (rand3 <= 0.25 * ratio)
        {
            infected = 1;
        }
        else if (rand3 > 0.25 * ratio && rand3 <= ratio)
        {
            infected = 2;
        }
    }

    // Update is called once per frame
    void Update()
    {
        isStopped = gameObject.GetComponent<NavMeshAgent>().isStopped;
        
    }
    
}
