using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CreateCapsule : MonoBehaviour
{
    public Material healthy;
    public MeshRenderer rend;

    public GameObject preFab;
    // Start is called before the first frame update
    void Start()
    {
        // Adjust for loop range to choose how many characters to spawn
        for (int i = 0; i < 20; i++)
        {
            //GameObject newGameObject = Instantiate(preFab, new Vector3(0, 1, -4 + 2*i), Quaternion.identity);
            GameObject newGameObject = Instantiate(preFab, new Vector3(0, 1, -4), Quaternion.identity);
            newGameObject.name = "Climber " + i;
        }
    }
    
    
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
