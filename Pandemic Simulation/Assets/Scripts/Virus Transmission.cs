using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirusTransmission : MonoBehaviour
{
    private List<GameObject> gameobjects;
    private int infected_count;
    private int infectious_count;
    
    // Start is called before the first frame update
    void Start()
    {
        infected_count = 0;
        infectious_count = 0;
        GameObject[] temp = GameObject.FindGameObjectsWithTag("Climber");
        gameobjects = new List<GameObject>(temp);
        if (gameObject.name == "Climber 1")
        {
            PrintInfectedFromStart(); 
        }
    }
    
    // Function which returns an array of climbers within radius to be infected
    public List<AgentClass> AgentsInInfectionRadius(AgentClass agent, int radius)
    {
        var position = agent.transform.position;
        
        List<AgentClass> agents_in_range = new List<AgentClass>();

        foreach (GameObject climber in gameobjects)
        {
            if (climber.GetComponent<AgentClass>().infected != 2 &&
                climber.GetComponent<AgentClass>().infected != 1)
            {
                if (Vector3.Distance(position, climber.transform.position) <= radius)
                {
                    agents_in_range.Add(climber.GetComponent<AgentClass>());
                }
                else
                {
                    climber.GetComponent<AgentClass>().SetTriedInfected(0);
                }
            }
            
        }
        return agents_in_range;
    }
    
    // A function which infects climbers in the radius and takes into account vaccination status
    public void InfectClimbersInRadius(List<AgentClass> agent_list, int radius)
    {
        foreach (var climber in agent_list)
        {
            if (climber.infected != 2 && climber.infected != 1)
            {
                if (climber.tried_infected == false)
                {
                    climber.SetTriedInfected(1);
                    // Get a random value to determine probability of infection
                    float rand = Random.value;
                    // Probability for radius of 1, depending on vaccination status
                    if (radius == 1)
                    {
                        if (climber.vaccinated)
                        {
                            if (rand <= 0.005)
                            {
                                climber.SetMaterial(climber, climber.vaccinated, 1);
                                climber.SetInfected(1);
                            }
                        }
                        else
                        {
                            if (rand <= 0.01)
                            {
                                climber.SetMaterial(climber, climber.vaccinated, 1);
                                climber.SetInfected(1);
                            }
                        }
                    }
                    else if (radius == 2) // Probability for radius of 2 = 0.1%, or 0.001% if the climber is vaccinated
                    {
                        if (climber.vaccinated)
                        {
                            if (rand <= 0.00001)
                            {
                                climber.SetMaterial(climber, climber.vaccinated, 1);
                                climber.SetInfected(1);
                            }
                        }
                        else
                        {
                            if (rand <= 0.001)
                            {
                                climber.SetMaterial(climber, climber.vaccinated, 1);
                                climber.SetInfected(1);
                            }
                        }
                    }
                }
            }
            //Debug.Log($"{climber.name} with infected = {climber.Display_Infected} got infected by {agent.name}");
            
        }
    }

    void PrintInfectedFromStart()
    {
        foreach (GameObject climber in gameobjects)
        {
            if (climber.GetComponent<AgentClass>().infected == 1)
            {
                infected_count++;
            }

            else if (climber.GetComponent<AgentClass>().infected == 2)
            {
                infectious_count++;
            }
        }
        Debug.Log("Infected Climbers from start: " + infected_count);
        Debug.Log("Infectious Climbers from start: " + infectious_count);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
