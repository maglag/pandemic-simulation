using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Route : MonoBehaviour
{

    public int difficulty;
    public int time_to_complete;
    public bool occupied;
    public AgentClass agent;
    private string[] difficulties = {"D1", "D2", "D3", "D4"};
    
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 4; i++)
        {
            if (gameObject.name.Contains(difficulties[i]))
            {
                difficulty = i+1;
            }
        }
        if (difficulty == 1)
        {
            time_to_complete = Random.Range(15, 25);
        }
        else if (difficulty == 2)
        {
            time_to_complete = Random.Range(20, 25);
        }
        else if (difficulty == 3)
        {
            time_to_complete = Random.Range(20, 30);
        }
        else if (difficulty == 4)
        {
            time_to_complete = Random.Range(25, 35);
        }
        
        
    }

    public void SetOccupied()
    {
        occupied = true;
    }

    public void FreeOccupied()
    {
        occupied = false;
    }

    public void SetAgent(AgentClass current_agent)
    {
        agent = current_agent;
    }

    public void FreeAgent()
    {
        agent = null;
    }
    
    // Update is called once per frame
    void Update()
    {

    }
}
