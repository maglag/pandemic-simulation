using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climber
{
    
    public string name;
    public int skill_level; // Skill level from 1-4
    public int stamina; // How many attempts
    public int status; // 0 = Unvaccinated and healthy, 1 = vaccinated and healthy
    public int infected; // 0 = No, 1 = Got infected (not infectious because of incubation), 2 = Infectious
    public Climber(int skill, int stat, int infect)
    {
        skill_level = skill;
        if (skill == 1)
        {
            stamina = 15;
        }
        else if (skill == 2)
        {
            stamina = 20;
        }
        else
        {
            stamina = 25;
        }
        status = stat;
        infected = infect;
    }
    
    

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
